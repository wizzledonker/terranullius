#version 120

attribute vec3 position;
attribute vec3 normal;

//layout(location = 0) in vec3 position;
//layout(location = 1) in vec3 normal;
varying vec3 vertPosition;

//Basic water shader implementation
void main() {
    vertPosition = vec3(gl_Vertex);
    gl_Position = gl_ModelViewProjectionMatrix*gl_Vertex;
}