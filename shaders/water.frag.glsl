#version 120

uniform sampler2D tex_diffuse;

uniform int texture_index;
uniform vec3 viewpoint;

uniform float fogMin;
uniform float fogMax;

//The details of the texture itself
int textures_index = 5;
float texture_scale = 36.0;

vec3 fogColor = vec3(0.52, 0.8, 0.95); //Colour of the fog, hard coded atm

varying vec3 vertPosition;

float fogFactor(float fogMaxf) {
    float distance = distance(viewpoint*-1, vertPosition);
    return 1-clamp((fogMaxf-distance)/(fogMaxf-fogMin), 0.0, 1.0); //Clamp the fog, we're being very linear at the moment
}

void main() {

    float fogMaxF = fogMax*0.5; //Water is always in the middle

    vec4 landTexture = texture2D(tex_diffuse, vec2(((mod(vertPosition.x, texture_scale)/texture_scale)/textures_index)+texture_index*(1/float(textures_index)),
                                                   ((mod(vertPosition.z, texture_scale)/texture_scale)/textures_index)));
    gl_FragColor = mix(vec4(landTexture[0], landTexture[1], landTexture[2], 0.4), vec4(fogColor, 1.0), fogFactor(fogMaxF));
}