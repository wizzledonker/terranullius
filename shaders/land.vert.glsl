#version 120

attribute vec3 normal;

varying vec3 theNormal;
varying vec3 vertPosition;


void main(){
    theNormal = normal;
    vertPosition = vec3(gl_Vertex); 
    gl_Position = gl_ModelViewProjectionMatrix*gl_Vertex;
}