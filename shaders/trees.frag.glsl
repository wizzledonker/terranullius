#version 120

uniform sampler2D tex_diffuse;
uniform vec3 viewpoint;

//Fog variables
uniform float fogMin;
uniform float fogMax;

uniform float lightLevel;

float fog_range = 1024;
float sea_level = -4.0;

float treeTexPosX = 3.0;
float treeTexPosY = 2.0;

float treeSizeX = 2.0;
float treeSizeY = 3.0;

int textures_index = 5;

varying vec3 vertPosition;

//In our case, the vertex normal is actually the terrain normal
varying vec3 vertNormal;

vec3 lightDirection = vec3(1.4, -1.0, 0);

float fogFactor(float fogMaxFinal, float distance) {
    return 1-clamp((fogMaxFinal-distance)/(fogMaxFinal-fogMin), 0.0, 1.0); //Clamp the fog, we're being very linear at the moment
}

void main() {
    vec3 fogColor = vec3(0.52, 0.8, 0.95);

    float fogMaxF = fogMax;

    //Underwater fog
    fogMaxF = fogMaxF*(clamp((vertPosition.y-(sea_level-(fog_range/2)))/fog_range, 0.1, 1.0));

    float distance = distance(viewpoint*-1, vertPosition);

    //Find the light level
    float diffuseLight = clamp(dot(vertNormal, normalize(-lightDirection))+0.4, 0.0, 1.0);
    
    //The tree itself
    vec4 colour = texture2D(tex_diffuse, vec2(1-((1/textures_index)*treeTexPosX+gl_TexCoord[0].s*(treeSizeX/textures_index)), 1-((1/textures_index)*treeTexPosY+gl_TexCoord[0].t*(treeSizeY/textures_index))));

    gl_FragColor = vec4(mix(vec3(colour)*diffuseLight*lightLevel, fogColor, fogFactor(fogMaxF, distance)), colour.a);
}