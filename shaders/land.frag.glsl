#version 120

//Landscape fragment shader for Terra Nullius

//The terrain Diffuse texture
uniform sampler2D tex_diffuse;
uniform vec3 viewpoint;

uniform float fogMin;
uniform float fogMax;

float fog_range = 1024.0;
float water_falloff = 12.0;
float sea_level = -4.0;

//The details of the texture itself
int textures_index = 5;

//Grass co-ordinates
int texture_xindex = 0;
int texture_yindex = 0;

//Sand co-ordinates
int texture_sand_xindex = 0;
int texture_sand_yindex = 1;

//Cliff co-ordinates
int texture_cliff_xindex = 0;
int texture_cliff_yindex = 2;

//Grassy snow co-ordinates
int texture_snow_grassy_xindex = 0;
int texture_snow_grassy_yindex = 3;

//Snow co-ordinates
int texture_snow_xindex = 0;
int texture_snow_yindex = 4;

float texture_scale = 8.0;

varying vec3 vertPosition;
varying vec3 theNormal;

vec3 lightDirection = vec3(1.4, -1.0, 0);
vec3 lightColor = vec3(1.0, 1.0, 1.0);

vec3 worldDirection = vec3(0.0, -1.0, 0.0);

float clampValue(in float value) {
    return pow((value - 0.5), 1/3)*0.6+0.5;
}

float fogFactor(float fogMaxFinal, float distance) {
    return 1-clamp((fogMaxFinal-distance)/(fogMaxFinal-fogMin), 0.0, 1.0); //Clamp the fog, we're being very linear at the moment
}

void main(){
    vec3 Ke = vec3(0.0, 0.0, 0.0);
    vec3 Ka = vec3(0.3, 0.3, 0.3);
    vec3 Kd = vec3(0.6, 0.6, 0.6);

    vec3 fogColor = vec3(0.52, 0.8, 0.95);

    //Emmission Amount
    vec3 emissive = Ke;

    //Ka... first multiple is the ambient setting
    vec3 ambient = Ka*fogColor;

    vec3 norm = theNormal;

    vec3 L = normalize(-lightDirection);
    float diffuseLight = max(dot(norm,L), 0);

    diffuseLight *= clamp(((vertPosition.y-(sea_level-water_falloff)))/(water_falloff), 0.2, 1.0); //Underwater

    //Calculate where the normal faces
    vec3 w = -worldDirection;
    float slopeFactor = dot(norm, w);

    //The diffuse (First multiple)
    vec3 diffuse = Kd*lightColor*diffuseLight;

    vec3 light = emissive+ambient+diffuse;

    int xindex = texture_xindex;
    int yindex = texture_yindex;

    float modifier = sin(vertPosition.x/20)*10+sin(vertPosition.z/20)*5;

    //For our sand
    if ((vertPosition.y < 5.0+modifier/4) && (slopeFactor > 0.9)) {
        xindex = texture_sand_xindex;
        yindex = texture_sand_yindex;
    }


    //For the grassy snow
    if ((vertPosition.y > 180.0+modifier)) {
        xindex = texture_snow_grassy_xindex;
        yindex = texture_snow_grassy_yindex;
    }

    //For the snow
    if ((vertPosition.y > 256.0+modifier)) {
        xindex = texture_snow_xindex;
        yindex = texture_snow_yindex;
    }

    //For the cliffs
    if (slopeFactor < 0.72) {
        xindex = texture_cliff_xindex;
        yindex = texture_cliff_yindex;
        texture_scale = texture_scale*10;
    }

    float fogMaxF = fogMax;

    //Underwater fog
    fogMaxF = fogMaxF*(clamp((vertPosition.y-(sea_level-(fog_range/2)))/fog_range, 0.1, 1.0));
    //fogColor = vec3(0.1, 0.1, 1.0);

    float distance = distance(viewpoint*-1, vertPosition);

    //Texture of the landscape
    vec4 landTexture = texture2D(tex_diffuse, vec2(((mod(vertPosition.x, texture_scale)/texture_scale)/textures_index)+xindex*(1/float(textures_index)),
                                                   ((mod(vertPosition.z, texture_scale)/texture_scale)/textures_index)+yindex*(1/float(textures_index))));

    //Use fog in shader
    gl_FragColor = mix(vec4(light, 1.0)*landTexture, vec4(fogColor, 1.0), fogFactor(fogMaxF, distance));
}