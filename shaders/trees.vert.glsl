#version 120

attribute vec3 position;
attribute vec3 normal;

//layout(location = 0) in vec3 position;
//layout(location = 1) in vec3 normal;
varying vec3 vertNormal;
varying vec3 vertPosition;

//Basic tree shader implementation
void main() {
    vertPosition = vec3(gl_Vertex);
    gl_TexCoord[0] = gl_MultiTexCoord0;
    vertNormal = normal;
    gl_Position = gl_ModelViewProjectionMatrix*gl_Vertex;
}