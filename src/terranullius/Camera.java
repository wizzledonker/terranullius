package terranullius;

import java.awt.Font;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import static org.lwjgl.util.glu.GLU.gluPerspective;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Winfried
 */
public class Camera {
    //3d vector to store the camera's position in
    private Vector3f    position    = null;
    //the rotation around the Y axis of the camera
    private float       yaw         = 0.0f;
    //the rotation around the X axis of the camera
    private float       pitch       = 0.0f;
    
    float dx        = 0.0f;
    float dy        = 0.0f;
    float dt        = 0.0f; //length of frame
    long lastTime  = 0; // when the last frame was
    long time      = 0;
    
    // The gravity that is affecting the player at all times
    float gravity = 50f;
    float timeFalling = 0.0f; //Time that the player has been falling for
    //Level of the ground (temporary, for debugging
    float groundLevel = 3.0f;
    
    //Height of the player in metres
    float playerHeight = 5f;
    
    //Jumping variables
    float timeJumping = 0.0f;
    float playerJumpHeight = 2.5f; //Height of jump in metres
    float playerCurrentJumpVelocity = 0.0f;
    boolean isJumping = false;
    
    public boolean cinematic = true;
    public float cinematicSpeed = 5.0f;
    
    float sprintFactor = 2.5f;
    float waterFactor = 0.5f;

    float playerSpeed = 14.0f; //move 100 units per second
    boolean flycam = false; //Whether to use as a flycam
    
    //Constructor that takes the starting x, y, z location of the camera
    public Camera(float x, float y, float z, boolean cinematic)
    {
        //instantiate position Vector3f to the x y z params.
        position = new Vector3f(x, y, z);
        this.setCinematic(cinematic);
    }
    
    //increment the camera's current yaw rotation
    public void yaw(float amount)
    {
        //increment the yaw by the amount param
        yaw += amount;
    }

    //increment the camera's current yaw rotation
    public void pitch(float amount)
    {
        //increment the pitch by the amount param
        pitch += amount;
    }
    
    public void setFlying(boolean flying) {
        this.flycam = flying;
    }
    
    public void toggleFlying() {
        this.flycam = !this.flycam;
    }
    
    //moves the camera forward relative to its current rotation (yaw)
    //Added pitch too! (go Win!)
    public void walkForward(float distance)
    {
        position.x -= distance * ((float)Math.sin(Math.toRadians(yaw)));
        position.z += distance * ((float)Math.cos(Math.toRadians(yaw)));
        if (flycam){
            position.y -= distance * ((float)Math.sin(Math.toRadians(pitch)));
        }
    }

    //moves the camera backward relative to its current rotation (yaw)
    public void walkBackwards(float distance)
    {
        position.x += distance * ((float)Math.sin(Math.toRadians(yaw)));
        position.z -= distance * ((float)Math.cos(Math.toRadians(yaw)));
        if (flycam) {
            position.y += distance * (float)Math.sin(Math.toRadians(pitch));
        }
    }

    //strafes the camera left relitive to its current rotation (yaw)
    public void strafeLeft(float distance)
    {
        position.x -= distance * ((float)Math.sin(Math.toRadians(yaw-90)));
        position.z += distance * ((float)Math.cos(Math.toRadians(yaw-90)));
    }

    //strafes the camera right relitive to its current rotation (yaw)
    public void strafeRight(float distance)
    {
        position.x -= distance * ((float)Math.sin(Math.toRadians(yaw+90)));
        position.z += distance * ((float)Math.cos(Math.toRadians(yaw+90)));
    }
    
    public void startJump() {
        this.isJumping = true;
        this.playerCurrentJumpVelocity = this.playerJumpHeight*this.gravity;
    }
    
    //translates and rotate the matrix so that it looks through the camera
    public void lookThrough()
            
    {
        
        //set the projection matrix back to the identity
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        
        gluPerspective(TerraNullius.config.getFOV(), (float) 800/600, 0.5f, TerraNullius.config.getFogEnd());
        
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glLoadIdentity();
        //roatate the pitch around the X axis
        GL11.glRotatef(pitch, -1.0f, 0.0f, 0.0f);
        //roatate the yaw around the Y axis
        GL11.glRotatef(((float) yaw), 0.0f, 1.0f, 0.0f);
        //translate to the position vector's location
        GL11.glTranslatef(position.x, position.y, position.z);

        //GL11.glFrustum (-1.0, 1.0, -1.0, 1.0, 0.2, 100.0);
    }
    
    public void setCinematic(boolean enabled) {
        this.cinematic = enabled;
        if (!cinematic) {
            Mouse.setGrabbed(true);
        } else {
            Mouse.setGrabbed(false);
        }
    }
    
    public void newPosition(Vector3f pos) {
        this.position = pos;
    }
    
    public Vector3f getPosition() {
        return this.position;
    }
    
    public void loop()
    {
        float mouseSensitivity = TerraNullius.config.getMouseSensitivity()/100;
        
        //hide the mouse
        time = Sys.getTime();
        dt = ((float) (time - lastTime))/((float) Sys.getTimerResolution());
        lastTime = time;
        //distance in mouse movement from the last getDX() call.
        dx = Mouse.getDX();
        //distance in mouse movement from the last getDY() call.
        dy = Mouse.getDY();

        //Check if the game is frozen
        if (!(dt > 1000)) {
            if (!cinematic) {
                
                float movementSpeed = this.playerSpeed;

                if (!flycam) {
                    this.groundLevel = -((Terrain) TerraNullius.rootNode.getChildren().toArray()[0]).getElevationAtPoint(this.position.x, this.position.z) - playerHeight;

                    boolean underwater = -this.position.y < (TerraNullius.config.getWaterLevel()-this.playerHeight*0.5);
                    
                    float finalGravity = this.gravity;
                    
                    //Player is underwater
                    if (underwater) {
                        movementSpeed *= this.waterFactor;
                        finalGravity = (float) (gravity*Math.pow(waterFactor, 2));
                        //Float the player when they hold space
//                        if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
//                            this.position.y -= movementSpeed*Math.pow(this.waterFactor, 5);
//                            finalGravity = 0f;
//                        }
                    }
                    
                    //Apply the effect of gravity to the player, and jumping
                    if (isJumping && !underwater) {
                        if (this.playerCurrentJumpVelocity > 0){
                            this.position.y -= this.playerCurrentJumpVelocity*dt;
                            this.playerCurrentJumpVelocity -= finalGravity * this.timeJumping;
                            timeJumping += dt;
                        } else {
                            isJumping = false;
                        }
                    } else {
                        if (this.position.y < this.groundLevel) {
                            this.timeFalling += dt;
                            this.position.y += (finalGravity*this.timeFalling)*dt;
                        } else {
                            if (this.timeFalling > 0) {
                                this.timeFalling = 0.0f;
                                this.timeJumping = 0.0f;
                            }
                            this.position.y = this.groundLevel;
                        }
                    }
                }

                //controll camera yaw from x movement fromt the mouse
                this.yaw(dx * mouseSensitivity);
                //controll camera pitch from y movement fromt the mouse
                this.pitch(dy * mouseSensitivity);
                
                //Player is sprinting
                if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT))//move forward
                {
                    movementSpeed *= this.sprintFactor;
                }
                
                //If using the flycam, it goes really fast
                if (flycam) {
                    movementSpeed = movementSpeed*20f;
                }

                //when passing in the distance to move
                //we times the movementSpeed with dt this is a time scale
                //so if its a slow frame u move more then a fast frame
                //so on a slow computer you move just as fast as on a fast computer
                if (Keyboard.isKeyDown(Keyboard.KEY_W))//move forward
                {
                    this.walkForward(movementSpeed*dt);
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_S))//move backwards
                {
                    this.walkBackwards(movementSpeed*dt);
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_A))//strafe left
                {
                    this.strafeLeft(movementSpeed*dt);
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_D))//strafe right
                {
                    this.strafeRight(movementSpeed*dt);
                }
            } else {
                this.yaw(this.cinematicSpeed*dt);
            }
        }

        //look through the camera before you draw anything
        this.lookThrough();

    }
}
