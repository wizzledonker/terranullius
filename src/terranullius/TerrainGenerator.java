
package terranullius;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import terranullius.util.SimplexNoise;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Winfried
 */
public class TerrainGenerator {
    int size;
    float gridSize;
    
    public TerrainGenerator(int size, float gridSize) {
        this.size = size;
        this.gridSize = gridSize;
    }
    
    public Terrain generateNew(int tileX, int tileY, float scale, float intensity, float octaves, float lunacarity) {
        //Generate a terrain
        ArrayList<TerrainQuad> faces = new ArrayList<TerrainQuad>();
        SimplexNoise noise = new SimplexNoise();
        
        Vector3f vec[][] = new Vector3f[size][size];
        for (int row = 0; row < size; ++row) {
            for (int column = 0; column < size; ++column) {
                float x = row*scale;
                float y = column*scale;
                float z = noise.noise(x, y)*intensity;
                
                for (int i = 1; i < octaves; ++i) {
                    float iteration = noise.noise(x/((float) Math.pow(lunacarity, i)), y/((float) Math.pow(lunacarity, i)));
                    //First time through generating... generate big hills
                    if (i < 3) {
                        z = z*iteration;
                    } else {
                        z = z+(iteration);
                    }
                }
                
                vec[row][column] = new Vector3f(row*this.gridSize, z, column*this.gridSize);
            }
        }
        
        //Sort vertices to faces
        for (int i = 0; i < size-1; ++i) {
            for (int j = 0; j < size-1; ++j) {
                Vector3f v1 = vec[i][j];
                Vector3f v2 = vec[i][j+1];
                Vector3f v3 = vec[i+1][j+1];
                TerrainFace fa = new TerrainFace(v1, v2, v3);
                
                //Other face
                Vector3f v4 = vec[i+1][j];
                faces.add(new TerrainQuad(fa, new TerrainFace(v1, v3, v4), i, j));
            }
        }
        return (new Terrain(faces, this.size, vec));
    }
}
