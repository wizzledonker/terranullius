
package terranullius.scenegraph;

import java.util.HashSet;
import java.util.Set;
import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Winfried
 */
public class Node {
    Vector3f position;
    Vector3f rotation;
    Vector3f scale;
    
    private Set<Node> children;
    
    public Node() {
        this(new Vector3f(0.0f, 0.0f, 0.0f), (new Vector3f(1.0f, 1.0f, 1.0f)), new Vector3f(0.0f, 0.0f, 0.0f));
    }
    
    public Node(Vector3f position, Vector3f scale, Vector3f rotation) {
        this(position, scale, rotation, null);
    }
    
    public Node(Vector3f position, Vector3f scale, Vector3f rotation, Set<Node> children) {
        this.position = position;
        this.scale = scale;
        this.rotation = rotation;
        
        //Children system
        this.children = new HashSet<>();
        if (children != null){
            this.children.addAll(children);
        }
        this.init();
    }
    
    public void translate(float x, float y, float z) {
        for (Node child : children) {
            child.translate(x, y, z);
        }
        this.position.translate(x, y, z);
    }
    
    public float posX() {
        return this.position.x;
    }
    public float posY() {
        return this.position.y;
    }
    public float posZ() {
        return this.position.z;
    }
    
    public void scale(float x, float y, float z) {
        for (Node child : children) {
            child.scale(x, y, z);
        }
        this.scale.x *= x;
        this.scale.y *= y;
        this.scale.z *= z;
    }
    
    public float scaleX() {
        return this.scale.x;
    }
    public float scaleY() {
        return this.scale.y;
    }
    public float scaleZ() {
        return this.scale.z;
    }
    
    public void rotate(float x, float y, float z) {
        for (Node child : children) {
            child.rotate(x, y, z);
        }
        this.rotation.x += x;
        this.rotation.y += y;
        this.rotation.z += z;
    }
    
    public float rotX() {
        return this.rotation.x;
    }
    public float rotY() {
        return this.rotation.y;
    }
    public float rotZ() {
        return this.rotation.z;
    }
    
    public void attachChild(Node child) {
        this.children.add(child);
    }
    
    public Set<Node> getChildren() {
        return children;
    }
    
    public void mainUpdate(float dt) {
        this.update(dt);
        for (Node child : this.getChildren()) {
            child.mainUpdate(dt);
        }
    }
    
    public void mainDispose() {
        this.dispose();
        for (Node child : this.getChildren()) {
            child.mainDispose();
        }
    }
    
    public void dispose() {
        //Override this
    }

    public void update(float dt) {
        //Override this
    }

    public void init() {
        //Override this
    }
}
