/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terranullius.config;

import java.io.Serializable;
import java.security.AccessController;
import java.security.PrivilegedAction;
import terranullius.TerraNullius;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.PixelFormat;

/**
 *
 * @author Winfried
 */
public class ConfigManager implements Serializable {
    int screen_width = 1280;
    int screen_height = 720;
    int[] aamodes = {0, 2, 4, 8, 16};
    int currentAAMode = 2;
    
    //Control related
    float field_of_view = 90.0f;
    float mouse_sensitivity = 5.0f;
    
    float landscape_resolution = 10.0f;
    float water_level = -4.0f;
    
    //Fog
    float fog_start = 0.0f;
    float fog_end = 7168.0f;
    
    boolean fullscreen = false;
    
    public int getScreenWidth() {
        return screen_width;
    }

    public int getScreenHeight() {
        return screen_height;
    }
    
    public float getFogStart() {
        return this.fog_start;
    }
    
    public float getFogEnd() {
        return this.fog_end;
    }
    
    public float getLandscapeResolution() {
        return this.landscape_resolution;
    }
    
    public float getWaterLevel() {
        return this.water_level;
    }
    
    public boolean getFullScreen() {
        return this.fullscreen;
    }
    
    public float getFOV() {
        return this.field_of_view;
    }
    
    public void changeFOV(float newFOV) {
        this.field_of_view = newFOV;
    }
    
    public void changeMouseSensitivity(float newMSS) {
        this.mouse_sensitivity = newMSS;
    }
    
    public float getMouseSensitivity() {
        return this.mouse_sensitivity;
    }
    
    public int[] getAvailableAA() {
        return this.aamodes;
    }
    
    public int getAASamples() {
        return this.aamodes[this.currentAAMode];
    }
    
    /**
     * Set the display mode to be used 
     * 
     * @param width The width of the display required
     * @param height The height of the display required
     * @param fullscreen True if we want fullscreen mode
     */
    public PixelFormat setDisplayMode(int width, int height, boolean fullscreen) {
        
        System.setProperty("LWJGL_DISABLE_XRANDR", "true");
        
        // return if requested DisplayMode is already set
        if ((Display.getDisplayMode().getWidth() == width) && 
            (Display.getDisplayMode().getHeight() == height) && 
        (Display.isFullscreen() == fullscreen)) {
            return null;
        }

        try {
            DisplayMode targetDisplayMode = null;

        if (fullscreen) {
            DisplayMode[] modes = Display.getAvailableDisplayModes();
            int freq = 0;

            for (int i=0;i<modes.length;i++) {
                DisplayMode current = modes[i];

            if ((current.getWidth() == width) && (current.getHeight() == height)) {
                if ((targetDisplayMode == null) || (current.getFrequency() >= freq)) {
                    if ((targetDisplayMode == null) || (current.getBitsPerPixel() > targetDisplayMode.getBitsPerPixel())) {
                    targetDisplayMode = current;
                    freq = targetDisplayMode.getFrequency();
                            }
                        }

                // if we've found a match for bpp and frequence against the 
                // original display mode then it's probably best to go for this one
                // since it's most likely compatible with the monitor
                if ((current.getBitsPerPixel() == Display.getDesktopDisplayMode().getBitsPerPixel()) &&
                            (current.getFrequency() == Display.getDesktopDisplayMode().getFrequency())) {
                                targetDisplayMode = current;
                                break;
                        }
                    }
                }
            } else {
                targetDisplayMode = new DisplayMode(width,height);
            }

            if (targetDisplayMode == null) {
                System.out.println("Failed to find value mode: "+width+"x"+height+" fs="+fullscreen);
                return null;
            }
            
            //Set our display resolutions
            this.screen_height = targetDisplayMode.getHeight();
            this.screen_width = targetDisplayMode.getWidth();
            this.fullscreen = fullscreen;

            Display.setDisplayMode(targetDisplayMode);
            Display.setFullscreen(fullscreen);
            
            if (this.currentAAMode != 0) {
                return new PixelFormat(targetDisplayMode.getBitsPerPixel(), 0, 24, 0, this.aamodes[this.currentAAMode]);
            }

        } catch (LWJGLException e) {
            System.out.println("Unable to setup mode "+width+"x"+height+" fullscreen="+fullscreen + e);
        }
        return null;
    }

}
