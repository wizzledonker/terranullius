/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terranullius;

import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import terranullius.scenegraph.Node;
import terranullius.util.ShaderProgram;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBSamplerObjects;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Vector3f;
import static org.lwjgl.util.glu.GLU.gluPerspective;
import org.lwjgl.util.vector.Vector;
import org.lwjgl.util.vector.Vector2f;

/**
 *
 * @author Winfried
 */
public final class Terrain extends Node {
    //FloatBuffer stores the terrain vertices in sets of three
    ArrayList<TerrainQuad> faces = new ArrayList<TerrainQuad>();
    Vector3f[][] heightMap;
    Vector3f[][] normalMap;
    
    int objectDisplayList;
    int size;
    
    //Vertices
    protected int vaoId;
    protected int vboId;
    
    //Normals
    protected int naoId;
    protected int nboId;
    
    //Textures
    protected int taoId;
    protected int tboId;
    
    //Textures
    protected int texId;

    private ShaderProgram prog;
    
    public Terrain(ArrayList<TerrainQuad> facesf, int size, Vector3f[][] heights) {
        this.faces.addAll(facesf);
        heightMap = heights;
        
        this.size = size;
        
        normalMap = new Vector3f[size][size];
        
        texId = ShaderProgram.loadTexture("assets/textures/textures.png", GL13.GL_TEXTURE0);
        
        gen();
        
        try {
            //Init all the shader stuff
            
            //Binding attributes
            //Map<Integer, String> binds = new HashMap();
            //binds.put(0, "position");
            //binds.put(1, "normal");
            
            prog = new ShaderProgram(ShaderProgram.readFile("./shaders/land.vert.glsl", Charset.defaultCharset()),
                    ShaderProgram.readFile("./shaders/land.frag.glsl", Charset.defaultCharset()), null);
            
            //prog.setUniformVec3(2, new Vector3f(1.8f, -1.0f, 0));
            //prog.setUniformVec3(3, new Vector3f(1.0f, 1.0f, 1.0f));
        } catch (Exception ex) {
            Logger.getLogger(Terrain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void update(float dt) {
        
        //GL11.glMatrixMode(GL11.GL_PROJECTION);
        //GL11.glLoadIdentity();

        //GL11.glMatrixMode(GL11.GL_MODELVIEW);
        //GL11.glLoadIdentity();

        GL11.glTranslatef(this.posX(), this.posY(), this.posZ());
        GL11.glScalef(this.scaleX(), this.scaleY(), this.scaleZ());
        
        //Rotation
        GL11.glRotatef(this.rotX(), 1.0f, 0, 0);
        GL11.glRotatef(this.rotY(), 0, 1.0f, 0);
        GL11.glRotatef(this.rotZ(), 0, 0, 1.0f);
        
        //Bind the texture to the texture location
        int loc = prog.getUniformLocation("tex_diffuse");
        int ploc = prog.getUniformLocation("viewpoint");
        
        //Bind the fog start and end points
        int fsloc = prog.getUniformLocation("fogMin");
        int feloc = prog.getUniformLocation("fogMax");
        
        prog.use();
              
        prog.setUniformi(loc, 0);
        prog.setUniformVec3(ploc, TerraNullius.cam.getPosition());
        
        prog.setUniformf(fsloc, TerraNullius.config.getFogStart());
        prog.setUniformf(feloc, TerraNullius.config.getFogEnd());
        
        // Bind to the VAO that has all the information about the quad vertices
        GL30.glBindVertexArray(vaoId);
        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1); //Normals
        
        //Enable texture
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.texId);
        
        GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, faces.size()*2*3);
        
        GL20.glDisableVertexAttribArray(0);
        GL20.glDisableVertexAttribArray(1);
        
        GL30.glBindVertexArray(0);
    }
    
    public Vector3f getNormalAtPoint(float worldX, float worldZ) {
        int[] ret = this.getGridLocation(worldX, worldZ);
        
        return this.normalMap[ret[0]][ret[1]];
    }
    
    public float getSlopeFactorAtPoint(float worldX, float worldZ) {
        Vector3f up = new Vector3f(0.0f, 1.0f, 0.0f);
        
        return 1-Vector3f.dot(up, this.getNormalAtPoint(worldX, worldZ));
    }
    
    public int[] getGridLocation(float worldX, float worldZ) {
        //Convert to local co-ordinates
        float terrainX = worldX - this.posX();
        float terrainZ = worldZ - this.posZ();
        
        float squareSize = TerraNullius.config.getLandscapeResolution();
        
        int[] buf = {(int) Math.floor(-terrainX/squareSize), (int) Math.floor(-terrainZ/squareSize)};
        //Which grid square the player resides inside
        return buf;
    }
    
    public float getElevationAtPoint(float worldX, float worldZ) {
        //Convert to local co-ordinates
        float terrainX = worldX - this.posX();
        float terrainZ = worldZ - this.posZ();
        
        float squareSize = TerraNullius.config.getLandscapeResolution();
        
        int[] ret = this.getGridLocation(worldX, worldZ);
        
        int gridX = ret[0];
        int gridZ = ret[1];
        //If outside the terrain
        if (gridX >= (this.size-1) || gridZ >= (this.size-1) || gridX < 0 || gridZ < 0) {
            return 0;
        }
        
        float xCoord = (-terrainX%squareSize)/squareSize;
        float zCoord = (-terrainZ%squareSize)/squareSize;
        float answer;
        
        if (xCoord <= (1-zCoord)) {
            //In one triangle
            answer = this.barryCentric(new Vector3f(0, heightMap[gridX][gridZ].y, 0),
                    new Vector3f(1, heightMap[gridX+1][gridZ].y, 0),
                    new Vector3f(1, heightMap[gridX+1][gridZ+1].y, 1), new Vector2f(xCoord, zCoord));
        } else {
            //We are in the other one
            answer = this.barryCentric(new Vector3f(1, heightMap[gridX + 1][gridZ].y, 0), new Vector3f(1,
                                            heightMap[gridX + 1][gridZ + 1].y, 1), new Vector3f(0,
                                            heightMap[gridX][gridZ + 1].y, 1), new Vector2f(xCoord, zCoord));
        }
        
        return answer;
    }
    
    public float barryCentric(Vector3f p1, Vector3f p2, Vector3f p3, Vector2f pos) {
            float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
            float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
            float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
            float l3 = 1.0f - l1 - l2;
            return l1 * p1.y + l2 * p2.y + l3 * p3.y;
    }

    
    public void gen() {
        //Store the vertices themselves
        FloatBuffer vertices = BufferUtils.createFloatBuffer(faces.size()*2*3*3);
        {
            for (TerrainQuad face : faces) {
                for (Vector3f vertex : face.t1.vertices) {
                    vertex.store(vertices);
                }
                for (Vector3f vertex : face.t2.vertices) {
                    vertex.store(vertices);
                }
            }
        };
        vertices.flip();
        
        //Store the normals
        FloatBuffer normals = BufferUtils.createFloatBuffer(faces.size()*2*3*3);
        {
            //Process all the normals then add them to the NormalMap so they can be processed
            for (TerrainQuad face : faces) {
                Vector3f normal1 = face.t1.getFaceNormalDirection();
                this.normalMap[face.x][face.z] = normal1;
                this.normalMap[face.x][face.z+1] = normal1;
                this.normalMap[face.x+1][face.z+1] = normal1;
                
                //Vector3f normal2 = face.t2.getFaceNormalDirection();
                this.normalMap[face.x+1][face.z] = normal1;
            }
            
            //Process the normals to be per-vertex rather than per-face
            for (int x = 1; x < size-1; ++x) {
                for (int z = 1; z < size-1; ++z) {
                    
                    //Add the four adjacent vectors together
                    Vector3f result = Vector3f.add(Vector3f.add(Vector3f.add(this.normalMap[x+1][z], this.normalMap[x-1][z], null), this.normalMap[x][z+1], null), this.normalMap[x][z-1], null);
                    
                    this.normalMap[x][z] = new Vector3f(result.x/4, result.y/4, result.z/4);
                    //System.out.println(this.normalMap[x][z]);
                }
            } 
            
            //Loop through and add the normals to the buffer
            for (int x = 0; x < size-1; ++x) {
                for (int z = 0; z < size-1; ++z) {
                                        
                    //The other
                    this.normalMap[x][z].store(normals);
                    this.normalMap[x][z+1].store(normals);
                    this.normalMap[x+1][z+1].store(normals);
                    
                    //One triangle
                    this.normalMap[x][z].store(normals);
                    this.normalMap[x+1][z+1].store(normals);
                    this.normalMap[x+1][z].store(normals);

                }
            }
        }
        normals.flip();
        
        
        
        //Setup a vertex array
        vaoId = GL30.glGenVertexArrays();
        GL30.glBindVertexArray(vaoId);
        
        //Create a VBO
        vboId = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertices, GL15.GL_STATIC_DRAW);
        //Put the VBO in the attributes list at position 0
        GL20.glEnableVertexAttribArray(0);
        
        GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 0, 0);
        GL20.glDisableVertexAttribArray(0);
        
        // Deselect our buffer
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        
        //Now for the fancy normal buffer
        nboId = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, nboId);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, normals, GL15.GL_STATIC_DRAW);
        
        GL20.glEnableVertexAttribArray(1);
        GL20.glVertexAttribPointer(1, 3, GL11.GL_FLOAT, false, 0, 0);
        GL20.glDisableVertexAttribArray(1);
        
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        GL30.glBindVertexArray(0);

    }
    
        
    @Override
    public void dispose() {
        // Disable the VBO index from the VAO attributes list

        //Delete texture
        GL11.glDeleteTextures(texId);
        
        // Delete the VBO
        GL15.glDeleteBuffers(vboId);

        // Delete the VAO
        GL30.glDeleteVertexArrays(vaoId);
    }
}
