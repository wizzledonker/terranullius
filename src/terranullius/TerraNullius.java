/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terranullius;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.layout.align.HorizontalAlign;
import de.lessvoid.nifty.nulldevice.NullSoundDevice;
import de.lessvoid.nifty.sound.openal.OpenALSoundDevice;
import de.lessvoid.nifty.render.batch.BatchRenderDevice;
import de.lessvoid.nifty.render.batch.spi.BatchRenderBackend;
import de.lessvoid.nifty.renderer.lwjgl.input.LwjglInputSystem;
import de.lessvoid.nifty.renderer.lwjgl.render.LwjglBatchRenderBackendFactory;
import de.lessvoid.nifty.sound.SoundSystem;
import de.lessvoid.nifty.spi.time.impl.FastTimeProvider;
import java.io.File;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.lwjgl.BufferUtils;
import terranullius.config.ConfigManager;
import terranullius.scenegraph.Node;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.PixelFormat;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Winfried
 */
public class TerraNullius {
    public static ConfigManager config;
    public static Node rootNode;
    public static Camera cam;
    
    //Time variables
    float dt = 0.0f;
    long lastTime = 0;
    
    //The UI
    public static Nifty nifty = null;
    
    public static boolean GAME_STARTED = false;
    public static boolean done = false;
    
    public TerraNullius() {
        this.config = new ConfigManager();
    }
    
    
    
    public void begin() {
        try {
            PixelFormat pixels = this.config.setDisplayMode(1280, 720, false);
            Display.create(pixels);
            Display.setTitle("TerraNullius Alpha 0.3b");
            
        } catch (LWJGLException ex) {
            Logger.getLogger(TerraNullius.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //Initialise the Camera and the root node
        rootNode = new Node();
        cam = new Camera(0, 0, 0, true);
        
        //Initialise the main GUI
        LwjglInputSystem inputSystem = new LwjglInputSystem();
        
        try {
            //Attempt to start the UI
            inputSystem.startup();
        } catch (Exception ex) {
            Logger.getLogger(TerraNullius.class.getName()).log(Level.SEVERE, null, ex);
        }
        BatchRenderBackend device = LwjglBatchRenderBackendFactory.create();
        
        //Initialise nifty sound
        OpenALSoundDevice sound = new OpenALSoundDevice();
        
        nifty = new Nifty(new BatchRenderDevice(device), sound, inputSystem, new FastTimeProvider());
        nifty.fromXml("MainF", getClass().getClassLoader().getResourceAsStream("GUI/Main.xml"), "Main");
        
//        if (!nifty.getSoundSystem().addMusic("title", "assets/music/main.ogg")) {
//            System.out.println("Failed loading main menu music.");
//        } else {
//            nifty.getSoundSystem().getMusic("title").play();
//        }
        
        //Generate the terrain and begin the game proper
        init();
        this.exitOnGLError("Error in initialisation");
        
        //Loop while our exit key isn't down
        while(!Display.isCloseRequested() && !done) {
            
            //Initialise the timer
            long time = Sys.getTime();
            float dt = ((float) (time - lastTime))/((float) Sys.getTimerResolution());
            lastTime = time;

            //Miscellaneous Enables
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            GL11.glEnable(GL11.GL_CULL_FACE);
            GL11.glEnable(GL11.GL_ALPHA_TEST);
            GL11.glEnable(GL11.GL_BLEND);
            GL11.glEnable(GL11.GL_TEXTURE_2D);
            
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

            GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
            GL11.glCullFace(GL11.GL_BACK);
            GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
            //Set the viewport colour
            GL11.glClearColor(0.52f, 0.8f, 0.95f, 1.0f);

            cam.loop();
            rootNode.mainUpdate(dt);

            this.exitOnGLError("Error in loopCycle");
            
            uiLoop();
            loop(dt);
            Display.update();
        }
        nifty.exit();
        Display.destroy();
        rootNode.dispose();
    }
    
    public void init() {

        //Generate the terrain and add it
        int size = 1024;
        TerrainGenerator gen = new TerrainGenerator(size, config.getLandscapeResolution());
        Terrain ter = gen.generateNew(0, 0, 0.00005f*(config.getLandscapeResolution()), 1024f, 5, 0.4f);
        
        WaterBody water = new WaterBody(size);
        ter.attachChild(water);
        
        float start = size/((1/config.getLandscapeResolution())*2);
        cam.newPosition(new Vector3f(-start, -ter.getElevationAtPoint(-start, -start)-36f, -start));
        
        rootNode.attachChild(ter);
        
        TreeGenerator trees = new TreeGenerator(size, 50000);
        ter.attachChild(trees);
    }
    
    public void uiLoop() {
        
        //Pause menpau opening and closing
        if (!cam.cinematic) {
            while (Keyboard.next()) {
                if (Keyboard.getEventKeyState()) {
                    if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE) {
                        System.out.println("The game was paused/unpaused.");
                        nifty.gotoScreen("Main");
                        cam.setCinematic(true);
                    }
                    if (Keyboard.getEventKey() == Keyboard.KEY_SPACE && (!cam.isJumping) && (cam.timeJumping == 0)) {
                        cam.startJump();
                    }
                    if (Keyboard.getEventKey() == Keyboard.KEY_F) {
                        cam.toggleFlying();
                    }
                }
            }
        }
        
        // update nifty
        nifty.update();

        // save your OpenGL state
        // (assuming you are in glMatrixMode(GL_MODELVIEW) all the time)
        GL11.glPushMatrix();
        GL11.glPushAttrib(GL11.GL_ALL_ATTRIB_BITS);
        
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0, config.getScreenWidth(), config.getScreenHeight(), 0, -1, 1);
        
        //Remove the shaders to render the UI
        GL20.glUseProgram(0);

        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glLoadIdentity();

        GL11.glDisable(GL11.GL_DEPTH_TEST);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_CULL_FACE);

        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glAlphaFunc(GL11.GL_NOTEQUAL, 0);

        GL11.glDisable(GL11.GL_LIGHTING);

        // render Nifty (this will change OpenGL state)
        nifty.render(false);

        // restore your OpenGL state
        GL11.glPopAttrib();
        GL11.glPopMatrix();
    }
    
    public void loop(float dt) {
        //Update the FPS counter
        if (!cam.cinematic) {
            nifty.getCurrentScreen().findElementById("fps").getRenderer(TextRenderer.class).setText("FPS: " + Math.round(1/dt));
            nifty.getCurrentScreen().findElementById("fps").getRenderer(TextRenderer.class).setTextHAlign(HorizontalAlign.left);
        }
    }
    
    public void exitOnGLError(String errorMessage) {
        int errorValue = GL11.glGetError();
         
        if (errorValue != GL11.GL_NO_ERROR) {
            String errorString = GLU.gluErrorString(errorValue);
            System.err.println("ERROR - " + errorMessage + ": " + errorString);
             
            if (Display.isCreated()) Display.destroy();
            System.exit(-1);
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Set the path for the native libraries (uncomment when distributing)
        //System.setProperty("org.lwjgl.librarypath", new File("natives").getAbsolutePath());
        
        // TODO code application logic here
        (new TerraNullius()).begin();
    }
    
}
