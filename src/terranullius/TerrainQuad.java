/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terranullius;

/**
 *
 * @author Winfried
 */
public class TerrainQuad {
    //TerrainQuad utility class for defining two terrain faces
    TerrainFace t1;
    TerrainFace t2;
    
    //Co-ordinates of quads
    int x;
    int z;
    
    public TerrainQuad(TerrainFace face1, TerrainFace face2, int x, int z) {
        t1 = face1;
        t2 = face2;

        this.x = x;
        this.z = z;
    }
}
