/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terranullius.uiControllers;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.controls.CheckBox;
import de.lessvoid.nifty.controls.DropDown;
import de.lessvoid.nifty.controls.SliderChangedEvent;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import terranullius.TerraNullius;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author Winfried
 */
public class MainScreenController implements ScreenController {
    public Nifty mainUI = null;
    public Screen current = null;
    
    Element quitElement;
    Element loginElement;
    Element optionsElement;

    @Override
    public void bind(Nifty nifty, Screen screen) {
        mainUI = nifty;
        current = screen;
        
        quitElement = mainUI.createPopup("popupExit");
        loginElement = mainUI.createPopup("popupLogin");
        optionsElement = mainUI.createPopup("popupOptions");
    }

    @Override
    public void onStartScreen() {
        //Add main menu operations here
        
        //Populate the list of resolutions
        DropDown selector = optionsElement.findNiftyControl("res", DropDown.class);
        ArrayList<DisplayMode> resolutions = new ArrayList();
        DisplayMode current = Display.getDisplayMode();
        resolutions.add(current);
        
        //Get the list of all possible resolutions
        try {
            for (DisplayMode disp : Display.getAvailableDisplayModes()) {
                //Only add if the frequency is above 60 and bpp is 32
                resolutions.add(disp);
            }
        } catch (LWJGLException ex) {
            Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        selector.addAllItems(resolutions);
        selector.selectItem(current);
        
        //Get the fullscreen status
        CheckBox fullscreen = optionsElement.findNiftyControl("fs", CheckBox.class);
        fullscreen.setChecked(TerraNullius.config.getFullScreen());
        
        //Populate the anti aliasing list
        DropDown aa = optionsElement.findNiftyControl("aa", DropDown.class);
        for (int i : TerraNullius.config.getAvailableAA()) {
            aa.addItem(i);
        }
        aa.selectItem(TerraNullius.config.getAASamples());
    }

    @Override
    public void onEndScreen() {
        //Add main menu end operations here
    }
    
    //Actually start the game
    public void start() {
        //TODO: Replace with game UI
        mainUI.gotoScreen("blank");
        
        //Replace the cinematic camera for the real camera
        TerraNullius.cam.setCinematic(false);
    }
    
    //Handler for FOV changes
    @NiftyEventSubscriber(id="fov")
    public void onFOVChanged(String id, SliderChangedEvent event) {
        TerraNullius.config.changeFOV(event.getValue());
    }
    
    //Handler for mouse sensitivity changes
    @NiftyEventSubscriber(id="mss")
    public void onMouseChanged(String id, SliderChangedEvent event) {
        TerraNullius.config.changeMouseSensitivity(event.getValue());
    }
    
    public void setRes() {
        //Set the proper resolution
        DropDown selector = optionsElement.findNiftyControl("res", DropDown.class);
        int width = ((DisplayMode) selector.getSelection()).getWidth();
        int height = ((DisplayMode) selector.getSelection()).getHeight();
        
        boolean fs = optionsElement.findNiftyControl("fs", CheckBox.class).isChecked();
        cancel();
        
        //Update the resolution
        TerraNullius.config.setDisplayMode(width, height, fs);
        //GL11.glScissor(0, 0, width, height);
        
        GL11.glViewport(0, 0, width, height);
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0, width, height, 0, -1, 1);
        
        mainUI.resolutionChanged();
        
        System.out.println("Resolution Changed to: " + mainUI.getRenderEngine().getRenderDevice().getWidth() + " x " + mainUI.getRenderEngine().getRenderDevice().getHeight());
    }
    
    //Three different popups that we can access from the menu
    public void options() {
        mainUI.showPopup(current, optionsElement.getId(), null);
    }
    
    public void quit() {
        mainUI.showPopup(current, quitElement.getId(), null);
    }
    
    public void login() {
        mainUI.showPopup(current, loginElement.getId(), null);
    }
    
    public void processLogin() {
        TextField username = current.findNiftyControl("username", TextField.class);
        current.findElementById("loginLabel").getRenderer(TextRenderer.class).setText("Logged in as: " + username.getRealText());
        cancel();
    }
    
    public void realQuit() {
        TerraNullius.done = true;
    }
    
    public void cancel() {
        mainUI.closePopup(current.getTopMostPopup().getId());
    }
    
}
