/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terranullius;

import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector3f;
import terranullius.scenegraph.Node;
import terranullius.util.ShaderProgram;

/**
 *
 * @author Winfried
 */
public class WaterBody extends Node {
    private int size = 512;
    
    private int vaoID = 0;
    private int vbID = 0;
    
    private int currentFrame = 0;
    private int waterNumFrames = 3;
    private int waterFrame = 1;
    private float currentFrameTime = 0.0f;
    private float animationSpeed = 6.0f; //Speed of the water animation in frames per second
    
    private ShaderProgram prog;
    
    public WaterBody(int size) {
        try {
            //Map<Integer, String> binds = new HashMap();
            //binds.put(0, "position");
            //binds.put(1, "normal");
            
            prog = new ShaderProgram(ShaderProgram.readFile("./shaders/water.vert.glsl", Charset.defaultCharset()), ShaderProgram.readFile("./shaders/water.frag.glsl", Charset.defaultCharset()), null);
        } catch (Exception ex) {
            Logger.getLogger(WaterBody.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.size = Math.round(size*TerraNullius.config.getLandscapeResolution());
        this.generate();
    }
    
    private void generate() {
        
        FloatBuffer vertices = BufferUtils.createFloatBuffer(18);
        float[] vertarray = new float[]{0.0f, TerraNullius.config.getWaterLevel(), 0.0f,
            0.0f, TerraNullius.config.getWaterLevel(), size,
            size, TerraNullius.config.getWaterLevel(), size,
            0.0f, TerraNullius.config.getWaterLevel(), 0.0f,
            size, TerraNullius.config.getWaterLevel(), 0.0f,
            size, TerraNullius.config.getWaterLevel(), size,};
        
        vertices.put(vertarray);
        vertices.flip();
        
        vaoID = GL30.glGenVertexArrays();
        GL30.glBindVertexArray(vaoID);
        
        vbID = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbID);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertices, GL15.GL_STATIC_DRAW);
        
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        GL30.glBindVertexArray(0);
    }
    
    @Override
    public void update(float dt) {
        GL11.glDisable(GL11.GL_CULL_FACE);
        //GL20.glUseProgram(0);
        //GL11.glLoadIdentity();
        prog.use();
        
        int indexLocation = prog.getUniformLocation("texture_index");
        
        if (dt < 1000) {
            this.currentFrameTime += dt;
        }
        
        if (this.currentFrameTime >= 1.0f/(this.animationSpeed)) {
            this.currentFrame = (this.currentFrame >= (this.waterNumFrames-1)) ? 0 : (this.currentFrame + 1);
            this.currentFrameTime = 0;
        }
        
        
        prog.setUniformi(indexLocation, (currentFrame + waterFrame));
        
        int fogLocation = prog.getUniformLocation("viewpoint");
        
                //Bind the fog start and end points
        int fsloc = prog.getUniformLocation("fogMin");
        int feloc = prog.getUniformLocation("fogMax");
        
        prog.setUniformVec3(fogLocation, TerraNullius.cam.getPosition());
        prog.setUniformf(fsloc, TerraNullius.config.getFogStart());
        prog.setUniformf(feloc, TerraNullius.config.getFogEnd());
        
        //Draw the water surface
        GL30.glBindVertexArray(this.vaoID);
        
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbID);
        GL20.glEnableVertexAttribArray(0);
        GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 0, 0);
        
        //Enable texture
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, ShaderProgram.getTextureId());
        
        GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, 7);
        
        GL20.glDisableVertexAttribArray(0);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        GL30.glBindVertexArray(0);
        
        GL11.glEnable(GL11.GL_CULL_FACE);
    }
    
    @Override
    public void dispose() {
        GL30.glDeleteVertexArrays(this.vaoID);
    }
}
