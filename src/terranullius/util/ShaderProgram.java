/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terranullius.util;

import de.matthiasmann.twl.utils.PNGDecoder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import static java.lang.Character.getName;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.FloatBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Map.Entry;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import static org.lwjgl.opengl.GL20.*;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Winfried
 */

public class ShaderProgram {
    protected static FloatBuffer buf16Pool;
    
    //Shader Attributes
    int vertex;
    int fragment;
    int program;
    
    private static int textureId;
    
    public ShaderProgram(String vertexShader, String fragmentShader, Map<Integer, String> attributes) throws LWJGLException {
        //compile the String source
        vertex = compileShader(vertexShader, GL_VERTEX_SHADER);
        fragment = compileShader(fragmentShader, GL_FRAGMENT_SHADER);

        //create the program
        program = glCreateProgram();

        //attach the shaders
        glAttachShader(program, vertex);
        glAttachShader(program, fragment);

        //bind the attrib locations for GLSL 120
        if (attributes != null)
            for (Entry<Integer, String> e : attributes.entrySet())
                glBindAttribLocation(program, e.getKey(), e.getValue());
        
        //link our program
        glLinkProgram(program);

        //grab our info log
        String infoLog = glGetProgramInfoLog(program, glGetProgrami(program, GL_INFO_LOG_LENGTH));

        //if some log exists, append it 
        if (infoLog!=null && infoLog.trim().length()!=0)
            System.out.println(infoLog);

        //if the link failed, throw some sort of exception
        if (glGetProgrami(program, GL_LINK_STATUS) == GL11.GL_FALSE)
            throw new LWJGLException(
                    "Failure in linking program. Error log:\n" + infoLog);

        //detach and delete the shaders which are no longer needed
        glDetachShader(program, vertex);
        glDetachShader(program, fragment);
        glDeleteShader(vertex);
        glDeleteShader(fragment);
    }

    protected int compileShader(String source, int type) throws LWJGLException {
        //create a shader object
        int shader = glCreateShader(type);
        //pass the source string
        glShaderSource(shader, source);
        
        //compile the source
        glCompileShader(shader);

        //if info/warnings are found, append it to our shader log
        String infoLog = glGetShaderInfoLog(shader,
                glGetShaderi(shader, GL_INFO_LOG_LENGTH));
        if (infoLog!=null && infoLog.trim().length()!=0)
            System.out.println(getName(type) +": "+infoLog + "\n");

        //if the compiling was unsuccessful, throw an exception
        if (glGetShaderi(shader, GL_COMPILE_STATUS) == GL11.GL_FALSE)
            throw new LWJGLException("Failure in compiling " + getName(type)
                    + ". Error log:\n" + infoLog);

        return shader;
    }
    
    /**
     * Make this shader the active program.
     */
    public void use() {
        glUseProgram(program);
    }

    /**
     * Destroy this shader program.
     */
    public void destroy() {
        //a flag for GL -- the program will not actually be deleted until it's no longer in use
        glDeleteProgram(program);
    }

    /**
     * Gets the location of the specified uniform name.
     * @param str the name of the uniform
     * @return the location of the uniform in this program
     */
    public int getUniformLocation(String str) {
        return glGetUniformLocation(program, str);
    }
    
    /**
    * Sets the uniform data at the specified location (the uniform type may be int, bool or sampler2D). 
    * @param loc the location of the int/bool/sampler2D uniform 
    * @param i the value to set
    */
   public void setUniformi(int loc, int i) {
       if (loc==-1) return;
       glUniform1i(loc, i);
   }
   
   public void setUniformf(int loc, float i) {
       if (loc==-1) return;
       glUniform1f(loc, i);
   }
   
    public void setUniformVec3(int loc, Vector3f vec) {
       GL20.glUniform3f(loc, vec.x, vec.y, vec.z);
   }

   /**
    * Sends a 4x4 matrix to the shader program.
    * @param loc the location of the mat4 uniform
    * @param transposed whether the matrix should be transposed
    * @param mat the matrix to send
    */
   public void setUniformMatrix(int loc, boolean transposed, Matrix4f mat) {
       if (loc==-1) return;
       if (buf16Pool == null)
           buf16Pool = BufferUtils.createFloatBuffer(16);
       buf16Pool.clear();
       mat.store(buf16Pool);
       buf16Pool.flip();
       glUniformMatrix4(loc, transposed, buf16Pool);
   }
   
   public static int loadTexture(String filename, int slot) {
        int t1width = 0;
        int t1height = 0;
        
        ByteBuffer tex1 = null;
       
        try {
            // Open the PNG file as an InputStream
            InputStream in = ShaderProgram.class.getClassLoader().getResourceAsStream(filename);
            // Link the PNG decoder to this stream
            PNGDecoder decoder = new PNGDecoder(in);

            // Get the width and height of the texture
            t1width = decoder.getWidth();
            t1height = decoder.getHeight();


            // Decode the PNG file in a ByteBuffer
            tex1 = ByteBuffer.allocateDirect(
                    4 * decoder.getWidth() * decoder.getHeight());
            decoder.decode(tex1, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);
            tex1.flip();

            in.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        
                
        //Now we are going to create the texture buffer
        int texId = GL11.glGenTextures();
        GL13.glActiveTexture(slot);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texId);
        
        //Type of RGB stored in the file
        GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);

        //Setup filtering, i.e. how OpenGL will interpolate the pixels when scaling up or down
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);

        //Setup wrap mode, i.e. how OpenGL will handle pixels outside of the expected range
        //Note: GL_CLAMP_TO_EDGE is part of GL12
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
        
        //Upload the texture and tell OpenGL what type it is, generate mipmaps
        GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, t1width, t1height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, tex1);
        GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
        
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
        
        textureId = texId;
        return texId;
   }
   
   public static int getTextureId() {
       return ShaderProgram.textureId;
   }
   
    public static String readFile(String path, Charset encoding) throws IOException {
      byte[] encoded = Files.readAllBytes(Paths.get(path));
      return new String(encoded, encoding);
    }
}
