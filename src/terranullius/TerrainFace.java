/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terranullius;

import java.util.ArrayList;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Winfried
 */
public class TerrainFace {
    public ArrayList<Vector3f> vertices = new ArrayList<Vector3f>(3);
    //boolean flipped = false;
    
    public TerrainFace(Vector3f v1, Vector3f v2, Vector3f v3) {
        vertices.add(v1);
        vertices.add(v2);
        vertices.add(v3);
        //this.flipped
    }
    
    public Vector3f getFaceNormalDirection() {
        
        Vector3f output = new Vector3f(0.0f, 0.0f, 0.0f);
        
        for (int i = 0; i < vertices.size(); ++i) {
            Vector3f current = vertices.get(i);
            Vector3f next = vertices.get((i+1)%vertices.size());
            
            output.x = output.x + (current.y-next.y)*(current.z+next.z);
            output.y = output.y + (current.z-next.z)*(current.x+next.x);
            output.z = output.z + (current.x-next.x)*(current.y+next.y);
        }
        //Return the resulting vector.;
        
        output.normalise();
        return output;
        
    }
}
