/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terranullius;

import java.nio.FloatBuffer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Quaternion;
import terranullius.scenegraph.Node;
import terranullius.util.ShaderProgram;

/**
 *
 * @author Winfried
 */
public class TreeGenerator extends Node {
    //Class for generating trees for TerraNullius, really cool if it works
    //Will work using VBO's. The VBO's will then be updated when trees are destroyed/etc
    int size;
    int num;
    
    int vboId;
    int vaoId;
    int nboId;
    
    //Texture buffer
    int tboId;
    
    private ShaderProgram prog;
    
    public TreeGenerator(int terrainSize, int numTrees) {
        try {
            //Binding attributes
            Map<Integer, String> binds = new HashMap();
            binds.put(0, "position");
            binds.put(1, "normal");
            
            prog = new ShaderProgram(ShaderProgram.readFile("./shaders/trees.vert.glsl", Charset.defaultCharset()), ShaderProgram.readFile("./shaders/trees.frag.glsl", Charset.defaultCharset()), binds);
        } catch (Exception ex) {
            Logger.getLogger(WaterBody.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.size = Math.round(terrainSize*TerraNullius.config.getLandscapeResolution());
        this.num = numTrees;
        gen();
    }
    
    private void gen() {
        //Random generator
        Random random = new Random();
        
        //Initialise our vertex buffer
        FloatBuffer vertices = BufferUtils.createFloatBuffer(this.num*8*3);
        FloatBuffer uvs = BufferUtils.createFloatBuffer(this.num*8*2);
        
        //We're using the normal buffer to store the terrain normal data
        FloatBuffer normals = BufferUtils.createFloatBuffer(this.num*8*3);
        
        Terrain ter = ((Terrain) TerraNullius.rootNode.getChildren().iterator().next());
        
        //For every tree that needs to be made
        for (int i = 0; i < this.num; ++i) {
            
            //Generate a random position
            float positionX = random.nextFloat()*((float) size); //+ local terrain coord
            float positionZ = random.nextFloat()*((float) size); //+ local terrain Y coord
            float positionY = ter.getElevationAtPoint(-positionX, -positionZ);
            
            Vector3f normal = ter.getNormalAtPoint(-positionX, -positionZ);
            float slope = ter.getSlopeFactorAtPoint(-positionX, -positionZ);
            
            if (positionY > TerraNullius.config.getWaterLevel()+10 && slope < 0.2) {
                float treeSize = (random.nextFloat()+0.05f)*50f;
                //Place the first square
                (new Vector3f(positionX-treeSize, positionY, positionZ)).store(vertices); //Bottom left
                (new Vector2f(0.0f, 0.0f)).store(uvs);
                (new Vector3f(positionX-treeSize, positionY+(3*treeSize), positionZ)).store(vertices); //Top Left
                (new Vector2f(0.0f, 1.0f)).store(uvs);
                (new Vector3f(positionX+treeSize, positionY+(3*treeSize), positionZ)).store(vertices); //Top right
                (new Vector2f(1.0f, 1.0f)).store(uvs);
                (new Vector3f(positionX+treeSize, positionY, positionZ)).store(vertices); //Bottom right
                (new Vector2f(1.0f, 0.0f)).store(uvs);

                //Place the second square
                (new Vector3f(positionX, positionY, positionZ-treeSize)).store(vertices); //Bottom left
                (new Vector2f(0.0f, 0.0f)).store(uvs);
                (new Vector3f(positionX, positionY+(3*treeSize), positionZ-treeSize)).store(vertices); //Top Left
                (new Vector2f(0.0f, 1.0f)).store(uvs);
                (new Vector3f(positionX, positionY+(3*treeSize), positionZ+treeSize)).store(vertices); //Top right
                (new Vector2f(1.0f, 1.0f)).store(uvs);
                (new Vector3f(positionX, positionY, positionZ+treeSize)).store(vertices); //Bottom right
                (new Vector2f(1.0f, 0.0f)).store(uvs);
                
                //Add the normals
                for (int j = 0; j < 8; ++j) {
                    normal.store(normals);
                }
                
            } else {
                --i;
            }
        }
        
        vertices.flip();
        uvs.flip();
        normals.flip();
        
        //The trees are now made, now we make the buffer
        vaoId = GL30.glGenVertexArrays();
        GL30.glBindVertexArray(vaoId);
        
        vboId = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertices, GL15.GL_STATIC_DRAW);
        GL20.glEnableVertexAttribArray(0);
        GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 0, 0);
        GL20.glDisableVertexAttribArray(0);
        
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        
        nboId = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, nboId);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, normals, GL15.GL_STATIC_DRAW);
        GL20.glEnableVertexAttribArray(1);
        GL20.glVertexAttribPointer(1, 3, GL11.GL_FLOAT, false, 0, 0);
        GL20.glDisableVertexAttribArray(1);
        
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        
        tboId = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, tboId);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, uvs, GL15.GL_STATIC_DRAW);
        GL11.glTexCoordPointer(2, GL11.GL_FLOAT, 0, 0);
        
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        
        GL30.glBindVertexArray(0);
    }
    
    @Override
    public void update(float dt) {
        GL11.glDisable(GL11.GL_CULL_FACE); 
        
        prog.use();
        
        //Uniforms for the shader (fog)
        int fogLocation = prog.getUniformLocation("viewpoint");
        
                //Bind the fog start and end points
        int fsloc = prog.getUniformLocation("fogMin");
        int feloc = prog.getUniformLocation("fogMax");
        
        int lightLoc = prog.getUniformLocation("lightLevel");
        
        prog.setUniformVec3(fogLocation, TerraNullius.cam.getPosition());
        prog.setUniformf(fsloc, TerraNullius.config.getFogStart());
        prog.setUniformf(feloc, TerraNullius.config.getFogEnd());
        prog.setUniformf(lightLoc, 0.8f);
        
        GL30.glBindVertexArray(this.vaoId);
        
        //Enable texture
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, ShaderProgram.getTextureId());
        
        GL11.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
        
        //Vertices
        GL20.glEnableVertexAttribArray(0);
        
        GL20.glEnableVertexAttribArray(1);
        
        GL11.glDrawArrays(GL11.GL_QUADS, 0, this.num*2);
        
        GL20.glDisableVertexAttribArray(1);
        
        GL20.glDisableVertexAttribArray(0);
        
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        GL30.glBindVertexArray(0);
        
        GL11.glEnable(GL11.GL_CULL_FACE);
    }

}
