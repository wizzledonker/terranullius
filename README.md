
### TerraNullis ###

TerraNullius is a landscape generator which aims to provide a sensory multiplayer experience as it connects to a remote server. People online can drop 'pins', which can be seen by people playing the game. These pins are objects that are downloaded as they are approached in the game.

### How do I get set up? ###

To use this repository, clone it and open it in NetBeans. Once there, you will need to add the libraries necessary.

### Who do I talk to? ###

(C) Win Holzapfel 2015